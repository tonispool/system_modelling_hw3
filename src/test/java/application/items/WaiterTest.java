package application.items;

import org.junit.Test;
import application.enums.Level;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class WaiterTest {
  @Test
  public void initialSalary200() throws Exception {
    assertThat(new Waiter("", "").getSalary(), is(200));
  }

  @Test
  public void experienceMediumSalary300() throws Exception {
    assertThat(new Waiter("", "").setExperience(Level.MEDIUM).getSalary(), is(300));
  }

  @Test
  public void experienceHighSalary400() throws Exception {
    assertThat(new Waiter("", "").setExperience(Level.HIGH).getSalary(), is(400));
  }

  @Test
  public void trainingCosts800() throws Exception {
    assertThat(new Waiter("", "").getNeededSumForTraining(), is(800));
  }

}
