package application.items;

import org.junit.Test;
import application.enums.Level;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ReputationTest {
  @Test
  public void initialReputationIs15Points() throws Exception {
    assertThat(new Reputation().getPoints(), is(15));
  }

  @Test
  public void lowerThan15ReputationIsLow() throws Exception {
    assertThat(new Reputation(14).getLevel(), is(Level.LOW));
  }

  @Test
  public void between15And29ReputationIsMedium() throws Exception {
    assertThat(new Reputation(15).getLevel(), is(Level.MEDIUM));
    assertThat(new Reputation(29).getLevel(), is(Level.MEDIUM));
  }

  @Test
  public void over29ReputationIsHigh() throws Exception {
    assertThat(new Reputation(30).getLevel(), is(Level.HIGH));
  }
}
