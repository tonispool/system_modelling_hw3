package application.items;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MenuTest {
  @Test
  public void hasFiveDishes() throws Exception {
    assertThat(new Menu().getDishes().size(), is(5));
  }

  @Test
  public void hasFiveBeverages() throws Exception {
    assertThat(new Menu().getBeverages().size(), is(5));
  }
}
