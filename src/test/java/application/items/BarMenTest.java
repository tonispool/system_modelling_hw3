package application.items;

import org.junit.Test;
import application.enums.Level;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BarMenTest {
  @Test
  public void initialSalary300() throws Exception {
    assertThat(new Barmen("", "").getSalary(), is(300));
  }

  @Test
  public void mediumExperienceSalary400() throws Exception {
    assertThat(new Barmen("", "").setExperience(Level.MEDIUM).getSalary(), is(400));
  }

  @Test
  public void highExperienceSalary500() throws Exception {
    assertThat(new Barmen("", "").setExperience(Level.HIGH).getSalary(), is(500));
  }

  @Test
  public void trainingCosts1200() throws Exception {
    assertThat(new Barmen("", "").getNeededSumForTraining(), is(1200));
  }
}
