package application.items;

import org.junit.Test;
import application.enums.FoodQuality;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BeverageTest {
  @Test
  public void lowQualityCosts1() throws Exception {
    assertThat(new Beverage("", 1).setQuality(FoodQuality.LOW).getIngredientsCost(), is(1));
  }

  @Test
  public void highQualityCosts3() throws Exception {
    assertThat(new Beverage("", 1).setQuality(FoodQuality.HIGH).getIngredientsCost(), is(3));
  }
}
