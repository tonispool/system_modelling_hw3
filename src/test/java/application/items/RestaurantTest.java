package application.items;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RestaurantTest {

  @Test
  public void initialBudgetTenThousand() throws Exception {
    assertThat(new Restaurant().getBudget(), is(10000));
  }

  @Test
  public void hasNineTables() throws Exception {
    assertThat(new Restaurant().getTables().size(), is(9));
  }

  @Test
  public void has3Waiters() throws Exception {
    long waiters = new Restaurant().getEmployees().stream().filter(e -> e instanceof Waiter).count();
    assertThat(waiters, is(3L));
  }

  @Test
  public void has1Chef() throws Exception {
    long chefCount = new Restaurant().getEmployees().stream().filter(e -> e instanceof Chef).count();
    assertThat(chefCount, is(1L));
  }

  @Test
   public void has1Barmen() throws Exception {
    long barmenCount = new Restaurant().getEmployees().stream().filter(e -> e instanceof Barmen).count();
    assertThat(barmenCount, is(1L));
  }

}
