package application.items;

import org.junit.Test;
import application.enums.FoodQuality;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DishTest {
  @Test
  public void lowQualityCosts1() throws Exception {
    assertThat(new Dish("", 1).setQuality(FoodQuality.LOW).getIngredientsCost(), is(3));
  }

  @Test
  public void highQualityCosts3() throws Exception {
    assertThat(new Dish("", 1).setQuality(FoodQuality.HIGH).getIngredientsCost(), is(10));
  }
}
