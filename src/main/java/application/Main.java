package application;

import java.io.IOException;

import application.gamesimulators.MainSimulator;
import application.utils.IOUtil;


public class Main {
  
  private static MainSimulator simulator;

  public static void main(String[] args) throws IOException {
    try {
      printBanner();   
      simulator = new MainSimulator();
      simulator.play();
    } catch (Exception e) {
		e.printStackTrace();
	} finally {
      IOUtil.close();
    }
  }

  private static void printBanner() {
    System.out.println("######################");
    System.out.println("#   RESTAURANT GAME  #");
    System.out.println("######################");
  }
}
