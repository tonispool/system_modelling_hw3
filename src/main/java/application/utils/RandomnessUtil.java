package application.utils;

import java.util.Random;

public class RandomnessUtil {
	private static final Random RANDOM = new Random();

	public static boolean isTrueWithin(double v) {
		return RANDOM.nextDouble() <= v;
	}
}
