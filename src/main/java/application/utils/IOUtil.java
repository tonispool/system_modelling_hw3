/**
 * @(#) IOUtil.java
 */

package application.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IOUtil {

	public static final BufferedReader reader = new BufferedReader(
			new InputStreamReader(System.in));
	public static final String TABS = "\t\t";

	public static String getStringInput() throws IOException {
		String input = reader.readLine();
		return input;
	}

	public static Integer getIntegerInput() throws IOException {
		Integer input = 0;
		while (true) {
			try {
				input = Integer.valueOf(reader.readLine());
				break;
			} catch (NumberFormatException e) {
				System.out.println("Input must be an integer. Try again!");
			}			
		}
		return input; 
	}

	public static Integer getConstrainedIntegerInput(Integer min, Integer max)
			throws IOException {
		Integer input = 0;
		while (true) {
			input = getIntegerInput();
			if (input >= min && input <= max) break;
			else System.out.println("Input must be between " + min + " and " + max);
		}
		return input;
	}

	public static void close() throws IOException {
		reader.close();
	}

}
