package application.gamesimulators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import application.enums.Level;
import application.items.Beverage;
import application.items.Client;
import application.items.Dish;
import application.items.Employee;
import application.items.Restaurant;
import application.items.Table;
import application.utils.IOUtil;

public class DaySimulator {
	
	private final Restaurant restaurant;
	private final List<Client> clients;

	public DaySimulator(Restaurant restaurant, List<Client> clients) 
	{
		this.restaurant = restaurant;
		this.clients = clients;
	}

	public void simulateDay() throws IOException 
	{
		int nrOfClients = getNrOfClientsVisiting();
		int nrOfTables = nrOfClients / 2;
		
		restaurant.assignTablesToWaiters(nrOfTables);
		trainEmployees();
		
		System.out.println("There are " + nrOfClients + " clients visiting today.");
		
		assignClientsToTables(nrOfClients);
		
		for (int table_idx = 0; table_idx < nrOfTables; table_idx++) {
			for (Client c : restaurant.getTables().get(table_idx).getSeatedClients()) {
				Dish dish = c.orderDish(restaurant);
				Beverage beverage = c.orderBeverage(restaurant);
				Integer r = adjustReputation(restaurant.getTables().get(table_idx), c, dish, beverage);
				System.out.println(c.getName() + " orders " + dish.getName() + " and " + beverage.getName() + ". " + c.getName() + " is " + (r < 0 ? "not " : "") + "satisfied");
			}
		}
		System.out.println("End of the day: reputation -- " + restaurant.getReputation().getPoints() + " (" + restaurant.getReputation().getLevel() + "), budget -- " + restaurant.getBudget());
		restaurant.clearTables();
	}

	private Integer getNrOfClientsVisiting() 
	{
		switch (restaurant.getReputation().getLevel()) {
		case LOW:
			return 4;
		case MEDIUM:
			return 10;
		case HIGH:
			return 18;
		}
		return 0;
	}
	
	private Integer adjustReputation(Table table, Client client, Dish dish, Beverage beverage) {
		Integer result = 0;
		if (client.satisfiedWithService(table.getServingWaiter())) {
			restaurant.getReputation().increase();
			result += 1;
		} else {
			restaurant.getReputation().decrease();
			result -= 1;
		}
	    if (client.satisfiedWithMenuItem(restaurant.getChef(), dish)) {
	    	restaurant.getReputation().increase();
	    	result += 1;
	    } else {
	    	restaurant.getReputation().decrease();
	    	result -= 1;
	    }
	    if (client.satisfiedWithMenuItem(restaurant.getBarmen(), beverage)) {
	    	restaurant.getReputation().increase();
	    	result += 1;
	    } else {
	    	restaurant.getReputation().decrease();
	    	result -= 1;
	    }
	    return result;
	}

	private void assignClientsToTables(Integer nrOfClients) {
		for (int i = 0; i < nrOfClients; i += 2) {
			List<Client> clientPair = new ArrayList<>();
			clientPair.add(clients.get(i));
			clientPair.add(clients.get(i + 1));
			restaurant.getTables().get(i / 2).assignClients(clientPair);
		}
	}

	private void trainEmployees() throws IOException {
	    System.out.println("Would you like to increase any employee's experience (y/n)?");
    	System.out.println("job" + IOUtil.TABS + "name"+ IOUtil.TABS + "current experience");
    	restaurant.getEmployees().stream().forEach(e -> System.out.println(e.getClass().getSimpleName() + IOUtil.TABS + e.getName() + IOUtil.TABS + e.getExperience()));
	    while (IOUtil.getStringInput().toLowerCase().equals("y")) {
	    	System.out.println("Enter the first name of the employee to train:");
	    	String name = IOUtil.getStringInput();
	    	Employee employee = restaurant.selectEmployeeByName(name);
	    	if (employee.getExperience() == Level.HIGH) {
	    		System.out.println(name + " already has the highest experience possible!");
	    	} else {
	    		int neededSum = employee.getNeededSumForTraining();
	    		restaurant.deductFromBudget(neededSum);
	    		System.out.println(neededSum + "€ deducted from budget (remaining: " + restaurant.getBudget() + "€)");
	    		employee.increaseExperience();
	    		System.out.println(employee.getName() + " has been trained to level " + employee.getExperience());
	    	}
	    	System.out.println("Would you like to increase any other employee's experience (y/n)?");
	    }
	}

}
