package application.gamesimulators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import application.enums.FoodQuality;
import application.enums.Level;
import application.items.Client;
import application.items.Player;
import application.items.Restaurant;
import application.utils.IOUtil;

public class MainSimulator {

	public static final int GAME_WEEKS = 6;
	public static final int ADDITIONAL_COSTS = 4000;

	private Restaurant restaurant;
	private Player player;
	private List<Client> clients = new ArrayList<>();
	private WeekSimulator weekSimulator;
	
	public MainSimulator() throws IOException {
		this.init();
		weekSimulator = new WeekSimulator(restaurant, clients);
		System.out.println("Restaurant " + restaurant.getName()
				+ " has been opened at " + restaurant.getLocation() + "!");
	}

	private void init() throws IOException {
		System.out.println("Enter you first name:");
		String name = IOUtil.getStringInput();
		System.out.println("Enter your surname:");
		String surname = IOUtil.getStringInput();
		System.out.println("Enter the name of your restaurant:");
		String restaurantname = IOUtil.getStringInput();
		System.out.println("Enter the city of your restaurant:");
		String city = IOUtil.getStringInput();
		System.out.println("Enter the address of your restaurant:");
		String address = IOUtil.getStringInput();

		player = new Player(name, surname);
		restaurant = new Restaurant(player, restaurantname, city, address);

	    decideMenuItemQualities();
	    decideHighAndLowQualityDishCost();
	    decideHighAndLowQualityBeverageCost();
	    
		 for (String clientName : Client.CLIENT_NAMES) { 
			 clients.add(new Client(clientName)); 
		 }
	}

	public void play() throws IOException {
		try {
			for (int i = 1; i <= GAME_WEEKS; i++) {
				System.out.println("##### WEEK NR " + i + " #####");
				weekSimulator.simulateWeek();
				System.out.println(restaurant.getBudget()
						+ "€ left after week " + i);
			}
			restaurant.deductFromBudget(ADDITIONAL_COSTS);
		} catch (IllegalStateException e) {
			gameOver(e);
		}
		gameOver(null);
	}

	private void gameOver(IllegalStateException e) {
		System.out.println("GAME OVER!");
		if (e != null) System.out.println(e.getMessage());
		else {
			System.out.println(player.getName() + " FINAL SCORE: "
					+ restaurant.getBudget());
			System.out.println("Statistics:");
			System.out.println("Client" + IOUtil.TABS + " Money spent" + IOUtil.TABS
					+ " Avg calories" + IOUtil.TABS + " Avg volume");
			clients.forEach(client -> System.out.println(client.getStatistics()));
		}
	}

	private void decideHighAndLowQualityBeverageCost() throws IOException {
	    System.out.println("How much does a HIGH quality beverage cost?");
	    Integer highCost = IOUtil.getConstrainedIntegerInput(0, 1000000);
	    System.out.println("How much does a LOW quality beverage cost?");
	    Integer lowCost = IOUtil.getConstrainedIntegerInput(0, 1000000);

	    restaurant.getMenu().getBeverages().forEach(beverage -> {
	      if (beverage.getQuality() == FoodQuality.LOW)
	        beverage.setPrice(lowCost);
	      else
	        beverage.setPrice(highCost);
	    });
	}

	private void decideHighAndLowQualityDishCost() throws IOException {
	    System.out.println("How much does a HIGH quality dish cost?");
	    Integer highCost = IOUtil.getConstrainedIntegerInput(0, 1000000);
	    System.out.println("How much does a LOW quality dish cost?");
	    Integer lowCost = IOUtil.getConstrainedIntegerInput(0, 1000000);

	    restaurant.getMenu().getDishes().forEach(dish -> {
	      if (dish.getQuality() == FoodQuality.LOW)
	        dish.setPrice(lowCost);
	      else
	        dish.setPrice(highCost);
	    });
	}

	private void decideMenuItemQualities() throws IOException {
		System.out.println("Altogether we have 5 dishes and 5 beverages.");
	    System.out.println("How many out of 5 dishes are high quality?");
	    Integer highQualityDishes = IOUtil.getConstrainedIntegerInput(0, 5);
	    restaurant.getMenu().getDishes()
	        .stream()
	        .limit(highQualityDishes)
	        .forEach(dish -> dish.setQuality(FoodQuality.HIGH));

	    System.out.println("How many out of 5 beverages are high quality?");
	    Integer highQualityBeverages = IOUtil.getConstrainedIntegerInput(0, 5);
	    restaurant.getMenu().getBeverages()
	        .stream()
	        .limit(highQualityBeverages)
	        .forEach(beverage -> beverage.setQuality(FoodQuality.HIGH));
	}	
}
