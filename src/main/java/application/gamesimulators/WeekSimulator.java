package application.gamesimulators;

import java.io.IOException;
import java.util.List;

import application.items.Client;
import application.items.Restaurant;

public class WeekSimulator {
	private static final int WEEK_DAYS = 5;
	private final Restaurant restaurant;
	private final DaySimulator daySimulation;

	public WeekSimulator(Restaurant restaurant, List<Client> clients) {
		this.restaurant = restaurant;
		daySimulation = new DaySimulator(restaurant, clients);
	}

	public void simulateWeek() throws IOException {
		for (int i = 1; i <= WEEK_DAYS; i++) {
			System.out.println("###### DAY NR " + i + " ######");
			daySimulation.simulateDay();
		}
		paySuppliers();
		paySalaries();
	}

	private void paySuppliers() {
		restaurant.deductFromBudget(restaurant.getTotalIngredientCost());
		restaurant.setTotalIngredientCost(0);
	}

	private void paySalaries() {
		restaurant.getEmployees().forEach(e -> restaurant.deductFromBudget(e.getSalary()));
	}
}
