/**
 * @(#) Dish.java
 */

package application.items;

import application.enums.FoodQuality;

public class Dish extends MenuItem {

  private Integer calorieCount;

  public Dish(String name, int calorieCount) {
    super(name);
    this.calorieCount = calorieCount;
  }

  public Integer getIngredientsCost() {
    if (quality == FoodQuality.HIGH) return 10;
    return 3;
  }

  public Integer getCalories() {
    return calorieCount;
  }
}