package application.items;

/**
 * @(#) Restaurant.java
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import application.items.Client;
import application.items.Employee;
import application.items.Menu;
import application.items.Player;
import application.items.Reputation;
import application.items.Table;
import application.items.Waiter;

public class Restaurant {

	private static final Integer NR_OF_TABLES = 9;

	private String name;

	private String address;

	private String city;

	private Player owner;

	private List<Waiter> waiters = new ArrayList<>();
	
	private Barmen barmen;
	
	private Chef chef;

	private Reputation reputation = new Reputation();

	private List<Table> tables = new ArrayList<>();

	private Menu menu = new Menu();

	private Integer budget = 10000;

	private Integer totalIngredientCost = 0;

	public Restaurant() 
	{
		for (int i = 0; i < NR_OF_TABLES; i++) {
			tables.add(new Table(i));
		}
		waiters.add(new Waiter("Sam", "Jackson"));
		waiters.add(new Waiter("John", "Smith"));
		waiters.add(new Waiter("Ken", "Dorn"));
		chef = new Chef("Vinny", "Best");
		barmen = new Barmen("Jimmy", "Walker");		
	}
	
	public Restaurant(Player owner, String name, String city, String address) {
		this();
		this.owner = owner;
		this.name = name;
		this.city = city;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return address + ", " + city;
	}

	public Integer getBudget() {
		return budget;
	}
	
	public Integer getTotalIngredientCost() 
	{
		return totalIngredientCost;
	}
	
	public void setTotalIngredientCost(Integer s)
	{
		totalIngredientCost = s;
	}
	
	public void addtoTotalIngredientCost(Integer s)
	{
		totalIngredientCost += s;
	}

	public Reputation getReputation()
	{
		return reputation;
	}
	
	public Employee selectEmployeeByName(String name) {
		return getEmployees().stream().filter(e -> e.getName().equals(name)).findFirst().get();
	}

	public void deductFromBudget(Integer sum) 
	{
		if (budget - sum <= 0) throw new IllegalStateException("Out of money");
		budget -= sum;
	}
	
	public void addToBudget(Integer sum)
	{
		budget += sum;
	}

	public List<Table> getTables() {
		return tables;
	}

	public List<Employee> getEmployees() {
		List<Employee> employees = new ArrayList<>();
		employees.addAll(waiters);
		employees.add(barmen);
		employees.add(chef);
		return employees;
	}

	public Menu getMenu() {
		return menu;
	}

	public Chef getChef() {
		return chef;
	}

	public Barmen getBarmen() {
		return barmen;
	}
	
	public List<Waiter> getWaiters ()
	{
		return waiters;
	}
	
	public void assignTablesToWaiters(Integer nrOfTables) {
		Collections.sort(waiters);
		int waiter_idx = 0;
		for (int i = 0; i < nrOfTables; i++) {
			tables.get(i).assignWaiter(waiters.get(waiter_idx));
			System.out.println("Table nr " + i + " was assigned to waiter " + waiters.get(waiter_idx).getName() + " (" + waiters.get(waiter_idx).getExperience() + ")");
			if ((i + 1) % 3 == 0) waiter_idx += 1;
		}
	}

	public void clearTables() {
		getTables().forEach(Table::clear);
	}
}