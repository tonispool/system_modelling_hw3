/**
 * @(#) Beverage.java
 */

package application.items;

import application.enums.FoodQuality;

public class Beverage extends MenuItem {
  private Integer volume;

  public Beverage(String name, int volume) {
    super(name);
    this.volume = volume;
  }

  public Integer getIngredientsCost() {
    if (quality == FoodQuality.HIGH) return 3;
    return 1;
  }

  public Integer getVolume() {
    return volume;
  }
}