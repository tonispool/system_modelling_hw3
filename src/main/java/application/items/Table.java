/**
 * @(#) Table.java
 */

package application.items;

import java.util.ArrayList;
import java.util.List;

public class Table {

	private Integer number;
	private Waiter servingWaiter;
	private List<Client> seated = new ArrayList<>();

	public Table(Integer number) 
	{
		this.number = number;
	}

	public Integer getNumber() 
	{
		return number;
	}

	public Waiter getServingWaiter() 
	{
		return servingWaiter;
	}
	
	public List<Client> getSeatedClients()
	{
		return seated;
	}

	/*public boolean isTableFree() 
	{
		return seated.isEmpty();
	}*/

	public void assignWaiter(Waiter waiter) 
	{
		servingWaiter = waiter;
	}

	public void assignClients(List<Client> clients) 
	{
		seated.addAll(clients);
	}

	/*public boolean isTableAssigned() 
	{
		return servingWaiter != null;
	}*/

	public void clear() 
	{
		seated.clear();
		servingWaiter = null;
	}
}
