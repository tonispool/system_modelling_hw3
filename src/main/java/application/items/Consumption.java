/**
 * @(#) Consumption.java
 */

package application.items;

import java.util.ArrayList;
import java.util.List;

import application.utils.IOUtil;

public class Consumption {
	
  private Integer moneySpent = 0;
  
  private List<Dish> dishesEaten = new ArrayList<>();
  
  private List<Beverage> consumedBeverages = new ArrayList<>();
  
  private Integer caloriesCount = 0;
  
  private Integer averageVolume = 0;

  public String getSummary() {
    return moneySpent
        + IOUtil.TABS +" " + round(caloriesCount.doubleValue() / dishesEaten.size(), 2)
        + IOUtil.TABS +" " + round(averageVolume.doubleValue() / consumedBeverages.size(), 2);
  }

  public void addDish(Dish randomDish) {
    moneySpent += randomDish.getPrice();
    caloriesCount += randomDish.getCalories();
    dishesEaten.add(randomDish);
  }

  public void addBeverage(Beverage randomBeverage) {
    moneySpent += randomBeverage.getPrice();
    averageVolume += randomBeverage.getVolume();
    consumedBeverages.add(randomBeverage);
  }

  private static double round (double value, int precision) {
    int scale = (int) Math.pow(10, precision);
    return (double) Math.round(value * scale) / scale;
  }
}
