/**
 * @(#) Chef.java
 */

package application.items;

import application.enums.Level;

public class Chef extends Employee {
  private int taxcode;

  public Chef(String name, String surname) {
    super(name, surname);
    baseSalary = 300;
  }

  @Override
  public Integer getNeededSumForTraining() {
    return 1200;
  }

  @Override
  public void updateSalary() {
    if (experience == Level.HIGH) baseSalary = 500;
    else if (experience == Level.MEDIUM) baseSalary = 400;
    else baseSalary = 300;
  }
}
