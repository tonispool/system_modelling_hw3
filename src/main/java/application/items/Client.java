/**
 * @(#) Client.java
 */

package application.items;

import application.enums.FoodQuality;
import application.utils.IOUtil;
import application.utils.RandomnessUtil;

public class Client extends Person {
	
  public static final String[] CLIENT_NAMES = new String[] {"Ariana Lora", "Saturnina Baltzell", "Vannessa Abila",
	      "Brice Falkowski", "Hsiu Gaudette", "Pamala Urbain", "Williemae Oldenburg", "Tamie Cordle",
	      "Delpha Walther","Toya Blanton","Amberly Kluge","Franchesca Baugh", "Pearlie Bellomy",
	      "Yelena Kleinschmidt","Raleigh Langford","Deandrea Magby", "Josiah Convery", "Loralee Ferrell"};
	
  private Integer telephone;
  private Integer taxcode;
  private Consumption statistics = new Consumption();

  public Client(String fullname) 
  {
    super(fullname.split(" ")[0], fullname.split(" ")[1]);
  }  

  public String getStatistics() 
  {
    return getName() + IOUtil.TABS + " " + statistics.getSummary();
  }

  public Dish orderDish(Restaurant restaurant) 
  {
	  Dish dish = restaurant.getMenu().getRandomDish();
	  restaurant.addToBudget(dish.getPrice());
	  restaurant.addtoTotalIngredientCost(dish.getIngredientsCost());
	  statistics.addDish(dish);
	  return dish;
  }
  
  public Beverage orderBeverage(Restaurant restaurant)
  {
	  Beverage beverage = restaurant.getMenu().getRandomBeverage();
	  restaurant.addToBudget(beverage.getPrice());
	  restaurant.addtoTotalIngredientCost(beverage.getIngredientsCost());
	  statistics.addBeverage(beverage);
	  return beverage;
  }

  public boolean satisfiedWithService(Waiter waiter) 
  {
    switch (waiter.getExperience()) {
    case LOW:
      return RandomnessUtil.isTrueWithin(0.6); // 60% of times
    case MEDIUM:
      return RandomnessUtil.isTrueWithin(0.8); // 80% of times
    case HIGH:
      return RandomnessUtil.isTrueWithin(0.9); // 90% of times
    }
    return false;
  }

  public boolean satisfiedWithMenuItem(Employee employee, MenuItem menuItem) 
  {
    switch (employee.getExperience()) {
    case LOW:
      return RandomnessUtil.isTrueWithin(getPercentage(menuItem, 0.4, 0.6));
    case MEDIUM:
      return RandomnessUtil.isTrueWithin(getPercentage(menuItem, 0.6, 0.8));
    case HIGH:
      return RandomnessUtil.isTrueWithin(getPercentage(menuItem, 0.8, 1.0));
    }
    return false;
  }

  private double getPercentage(MenuItem menuItem, double baseCase, double highCase) 
  {
    double unmodifiedPercentage = menuItem.getQuality() == FoodQuality.HIGH ? highCase : baseCase;
    int priceDifference = menuItem.getPrice() - menuItem.getIngredientsCost();
    double finalPercentage = unmodifiedPercentage - (Math.floorDiv(priceDifference, 3) * 0.1);
    return finalPercentage < 0.0 ? 0 : finalPercentage;
  }

}