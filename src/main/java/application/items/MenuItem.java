package application.items;

import application.enums.FoodQuality;

public abstract class MenuItem {
  public String getName() {
    return name;
  }

  protected String name;
  protected Integer price;
  protected FoodQuality quality = FoodQuality.LOW;
  
  public MenuItem(String name) {
    this.name = name;
  }

  public Integer getPrice() {
	return price;
  }
  
  public FoodQuality getQuality() {
    return quality;
  }

  public MenuItem setQuality(FoodQuality quality) {
    this.quality = quality;
    return this;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }
  
  public abstract Integer getIngredientsCost();
}
