/**
 * @(#) Waiter.java
 */

package application.items;

import application.enums.Level;

public class Waiter extends Employee {
	
	public Waiter(String name, String surname) {
		super(name, surname);
		baseSalary = 200;
	}

	@Override
	public Integer getNeededSumForTraining() {
		return 800;
	}

	@Override
	public void updateSalary() {
		if (experience == Level.HIGH) baseSalary = 400;
		else if (experience == Level.MEDIUM) baseSalary = 300;
		else baseSalary = 200;
	}
}
