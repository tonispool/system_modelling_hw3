/**
 * @(#) Reputation.java
 */

package application.items;

import application.enums.Level;

public class Reputation {
  private Integer points;

  public Reputation() {
    this(15);
  }

  public Reputation(Integer points) {
    this.points = points;
  }

  public Integer getPoints() {
	return points;
  }
  
  public Level getLevel() {
    if (points < 15) return Level.LOW;
    if (points < 30) return Level.MEDIUM;
    return Level.HIGH;
  }

  public void increase() {
    points++;
  }

  public void decrease() {
    points--;
  }
}
