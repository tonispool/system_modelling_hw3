/**
 * @(#) Person.java
 */

package application.items;

public abstract class Person {
  
  protected String name;
  private String surname;
  private Integer telephone;

  public Person(String name, String surname) {
    this.name = name;
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Person)) return false;

    Person person = (Person) o;

    if (name != null ? !name.equals(person.name) : person.name != null) return false;
    if (surname != null ? !surname.equals(person.surname) : person.surname != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (surname != null ? surname.hashCode() : 0);
    result = 31 * result + telephone;
    return result;
  }

  @Override
  public String toString() {
    return name + " " + surname;
  }
}
