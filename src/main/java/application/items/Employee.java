/**
 * @(#) Employee.java
 */

package application.items;

import application.enums.Level;

public abstract class Employee extends Person implements Comparable<Employee> {
  protected Integer baseSalary;
  protected Level experience = Level.LOW;
  
  public Employee(String name, String surname) {
    super(name, surname);
  }

  public Level getExperience() 
  {
    return experience;
  }
  
  public Employee setExperience(Level l)
  {
	  this.experience = l;
	  updateSalary();
	  return this;
  }
  
  public void increaseExperience() 
  {
    if (experience == Level.LOW) experience = Level.MEDIUM;
    else if (experience == Level.MEDIUM) experience = Level.HIGH;
    updateSalary();
  }
  
  public Integer getSalary()
  {
	  return baseSalary;
  }
  
  public int compareTo(Employee other)
  {
	  return -(this.experience.compareTo(other.experience));
  }
  
  public abstract Integer getNeededSumForTraining();
  public abstract void updateSalary();
  
}
