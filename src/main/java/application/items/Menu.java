/**
 * @(#) Menu.java
 */

package application.items;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class Menu {

	private static final Random RANDOM = new Random();
	private List<Beverage> drinks = new ArrayList<>();
	private List<Dish> dishes = new ArrayList<>();

	public Menu() {
		initDishes();
		initDrinks();
	}

	private void initDrinks() {
		drinks.add(new Beverage("Beer", 500));
		drinks.add(new Beverage("Soda", 750));
		drinks.add(new Beverage("Wine", 750));
		drinks.add(new Beverage("Water", 500));
		drinks.add(new Beverage("Juice", 500));
	}

	private void initDishes() {
		dishes.add(new Dish("Burger", 2000));
		dishes.add(new Dish("Pizza", 2500));
		dishes.add(new Dish("Fish", 1200));
		dishes.add(new Dish("Steak", 2200));
		dishes.add(new Dish("Soup", 1000));
	}

	public Collection<Beverage> getBeverages() {
		return drinks;
	}

	public Collection<Dish> getDishes() {
		return dishes;
	}

	public Dish getRandomDish() {
		return dishes.get(RANDOM.nextInt(dishes.size()));
	}

	public Beverage getRandomBeverage() {
		return drinks.get(RANDOM.nextInt(drinks.size()));
	}
}
